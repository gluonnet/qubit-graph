# Qubit Graph

Qubit Graph is a [Bloch sphere](https://en.wikipedia.org/wiki/Bloch_sphere) graph state visualisation library, developed by [gluoNNet](https://www.gluonnet.com) and members of the [CERN Webfest 2021 hackathon](https://webfest.cern).
It allows to visualise quantum computing [graph states](https://en.wikipedia.org/wiki/Graph_state) using Bloch spheres as nodes of the graph. The initial proof of concept (PoC) version was presented on the [22. August 2021](https://youtu.be/71WbR4_upm0?t=2880). Please find the Webfest video at [Youtube](https://www.youtube.com/watch?v=f8GFBSM9r1Y).

![](doc/Qubit-Graph-Overview.png)

## Demo

[Demo CERN Webfest 2021](https://codesandbox.io/s/qubit-graph-cern-webfest-2021-demo-7u5b5)

## Install

- ```npm install```

## Start

- ```npm start```

## Production Build

- ```npm run build```

## Features
- Visualise Block spheres
- Modify Bloch sphere axis tilt & rotation
- Visualise groups of nodes using colored Hulls
- Visualise a minimpap of the full graph
- Show legend and disable nodes o certain node type
- Save current view as .png through canvas right click menu: "Download Image"
- Save full view as .png through canvas right click menu: "Download Full Image"

## Usage

### Bloch Sphere Nodes

#### Parameters

| Parameters | Description | Required | Default/Value |
| ---------- | ----------- | -------- | ------------- |
| id | unique node identifier | yes | N/A |
| type | node type for Bloch spheres | yes | "quantum-node" | 
| theta | Theta angle of the qubit | yes | N/A |
| pie | Pie angle of the qubit | yes | N/A |
| x | x-position of the node | no | N/A |
| y | y-position of the node | no | N/A |
| size | size of the node | yes | 100 |
| label | node label, if no label provided id is used as label | no | N/A |
| color | color of the Bloch sphere node | no | "#fa6d01" |
| stateColor | color of the Bloch sphere state indictor | no | "red" |
| realProjection | boolean to use correct geometric projection for Bloch sphere, otherwise pseudo projection is used by default | no | false |
| projection | projection angles [z-axis tilt, z-axis rotation] of the Bloch sphere | no | [60, -20] |

## Contributors

- Daniel Dobos - daniel.dobos@cern.ch / daniel@gluonnet.com / @HScienceAdvisor
- Karolos Potamianos - karolos.potamianos@cern.ch / karolos@gluonnet.com / @kpotamianos
- Kristiane Novotny
